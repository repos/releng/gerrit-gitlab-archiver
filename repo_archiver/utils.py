def make_repo_name(repo):
    return repo.replace('/', '-')

def dry_run(dry_run=True):
    # Boolean logic is hard
    # If not a dry_run (i.e. dry_run=False), then return False
    if not dry_run:
        return False
    response = input("...actually run? [y/N] ")
    # If actually run is 'y', then it is not a dry run, so return False
    return response.lower() != 'y'


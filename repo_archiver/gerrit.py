import argparse
import datetime
import getpass
import json
import os
import subprocess
import tempfile
import urllib.parse as urlparse

import requests

from . import utils

GERRIT = 'gerrit.wikimedia.org'
GITLAB = 'gitlab.wikimedia.org'
ALL_ARCHIVED_PROJECTS = 'All-Archived-Projects'
SESSION = requests.Session()
DATE = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z")

class GerritArchiver:
    def __init__(self, args):
        self.args = args
        self.gerrit_url = f"https://{GERRIT}/r/"
        self.repo = args.repo.replace('/', '%2F')
        self.gerrit_remote = f"ssh://{GERRIT}:29418/{args.repo}"
        self.local_repo = tempfile.mkdtemp(prefix="gerritarchiver-")
        self.original_config = self._check_repo()
        # Per <https://gerrit-review.googlesource.com/Documentation/rest-api-projects.html#config-info>
        self.original_config.setdefault('description', '')
        self.original_config.setdefault('state', 'ACTIVE')
        self.repo_manage_url = urlparse.urljoin(
            self.gerrit_url, f'admin/repos/{args.repo},general')
        self.repo_view_url = urlparse.urljoin(
            self.gerrit_url, f'plugins/gitiles/{args.repo}')

        if self.original_config['state'] == "READ_ONLY":
            raise SystemExit(f"Repo already archived: {self.repo}")

    def archive(self):
        """
        Archive the repo. The meat of the script.
        """
        print(f"Gerrit: Archive\n\t{self.repo_manage_url}\n\t{self.repo_view_url}")
        self.update_parent()
        self.update_repo()
        self.update_description()

    def update_parent(self):
        """
        Reparent the repo to All-Archived-Projects

        This changes permissions so we can push (and changes the repo
        owner to `Gerrit Managers`)
        """
        print(f"Gerrit: Update parent of {self.repo} to {ALL_ARCHIVED_PROJECTS}")
        if self.dry_run():
            print("\tDry run, skipping...")
            return
        url = urlparse.urljoin(self.gerrit_url, f'a/projects/{self.repo}/parent')
        data = {"parent": ALL_ARCHIVED_PROJECTS}
        req = SESSION.put(url, json=data)
        req.raise_for_status()
        parent = req.text[5:].strip().strip('"')
        if parent != ALL_ARCHIVED_PROJECTS:
            raise Exception(f"Parent not updated: {parent}")

    def update_repo(self):
        """
        Create a local orphan branch, add a README, commit, and push.
        Then update the HEAD ref of the repo on gerrit to point to the
        new, orphan branch.
        """
        self.create_archive_repo()
        print(f"Gerrit: Push {self.local_repo} to {self.new_branch_name}")
        if self.dry_run():
            print("\tDry run, skipping...")
            return
        self._gitcmd("push", "-u", "gerrit", self.new_branch_name)
        self.update_repo_head()

    def create_archive_repo(self):
        """
        Create a local orphan branch, add a README, and commit it.

        the `new_branch_name` will depend on if we're archiving or moving
        to gitlab.

        The README will also vary depending on if we're archiving or moving.
        """
        self._gitcmd("init", f"--initial-branch={self.new_branch_name}")
        self._gitcmd("remote", "add", "gerrit", self.gerrit_remote)
        self.create_readme()
        self._gitcmd("add", "README.md")
        self._gitcmd("commit", "-m", f"{self.archive_message} by gerritarchiver.py")

    def create_readme(self):
        raise NotImplementedError

    def update_repo_head(self):
        """
        Change upstream HEAD to new_branch_name.

        new_branch_name will depend on if we're archiving or moving to gitlab.
        """
        url = urlparse.urljoin(self.gerrit_url, f'a/projects/{self.repo}/HEAD')
        data = {"ref": f"refs/heads/{self.new_branch_name}"}
        req = SESSION.put(url, json=data)
        req.raise_for_status()
        ref = req.text[5:].strip().strip('"')
        if ref != f"refs/heads/{self.new_branch_name}":
            raise Exception(f"HEAD not updated: {ref}")
        print(f"HEAD updated: {self.new_branch_name}")

    def update_description(self):
        """
        Grab the current description, add the archive message, and update.
        Finally, set the repo to read-only.
        """
        current_description = self.original_config['description']
        new_description = self.make_new_description(current_description)
        print(f"Gerrit: Update description of {self.repo} to {new_description}")
        if self.dry_run():
            print("\tDry run, skipping...")
            return
        self._make_read_only(new_description)

    def make_new_description(self, current_description):
        """
        Make a new repo description with the archive message and task (if any)
        """
        task = ""
        if self.args.task:
            task = f" ({self.args.task})"
        return f"{self.archive_message} {current_description}{task}"

    def dry_run(self):
        # Boolean logic is hard
        # if the user passed --yes, then self.args.yes is True
        # meaning we want to run the script
        # meaning it is not a dry run
        #
        # if the user did not pass --yes, then self.args.yes is False
        # meaning we do not want to run the script
        # meaning it is a dry run
        dry_run = not self.args.yes
        return utils.dry_run(dry_run)

    def _make_read_only(self, new_description):
        """
        Actually update the description and set the repo to read-only
        """
        url = urlparse.urljoin(self.gerrit_url, f'a/projects/{self.repo}/config')
        data = {
            "state": "READ_ONLY",
            "description": new_description,
        }
        req = SESSION.put(url, json=data)
        req.raise_for_status()
        updated = json.loads(req.text[5:])
        if updated['description'] != new_description:
            raise Exception(f"Description not updated: {updated['description']}")
        if updated['state'] != "READ_ONLY":
            raise Exception(f"State not updated: {updated['state']}")

    def _check_repo(self):
        """
        Get the current repo's config and check that it's not already archived
        """
        url = urlparse.urljoin(self.gerrit_url, f'a/projects/{self.repo}/config')
        req = SESSION.get(url)
        req.raise_for_status()
        # Strip the first five bytes of the Gerrit JSON response
        return json.loads(req.text[5:])

    def _gitcmd(self, *args):
        """
        Helper to run git commands on our named temp directory
        """
        return subprocess.check_output(
            ["git", "-C", self.local_repo, *args],
            text=True
        ).strip()



class GerritJustArchive(GerritArchiver):
    def __init__(self, args):
        super().__init__(args)
        self.new_branch_name = "ARCHIVED"
        self.archive_message = "[ARCHIVED]"

    def create_readme(self):
        task = ""
        if self.args.task:
            task_link = f"https://phabricator.wikimedia.org/{self.args.task}"
            task = f"[{self.args.task}]({task_link})\n"
        with open(os.path.join(self.local_repo, "README.md"), "w") as f:
            f.write(f"# {self.args.repo} Archived\n\n")
            f.write(f"- date: {DATE}\n")
            if self.args.task:
                f.write(f"- task: {task}\n")


class GerritMoveArchiver(GerritArchiver):
    def __init__(self, args):
        super().__init__(args)
        self.new_branch_name = "MOVED_TO_GITLAB"
        self.archive_message = "[MOVED TO GITLAB]"

    def create_readme(self):
        link = f"https://{GITLAB}/{self.args.gitlab_repo}"
        task = ""
        if self.args.task:
            task_link = f"https://phabricator.wikimedia.org/{self.args.task}"
            task = f"[{self.args.task}]({task_link})\n"
        with open(os.path.join(self.local_repo, "README.md"), "w") as f:
            f.write(f"# {self.args.repo} Moved to GitLab\n\n")
            f.write(f"- repo: [{self.args.gitlab_repo}]({link})\n")
            f.write(f"- date: {DATE}\n")
            if self.args.task:
                f.write(f"- task: {task}\n")


def build_archiver(args, username=None, password=None):
    if username and password:
        SESSION.auth = (username, password)
    if args.gitlab_repo is not None:
        return GerritMoveArchiver(args)
    return GerritJustArchive(args)

from phabricator import Phabricator

from . import utils

DRY_RUN = True
HOST = 'https://phabricator.wikimedia.org/api/'

class PhabArchiver:
    def __init__(self, repo, phab):
        self.repo = repo
        self.phab = phab
        self.repo_name = utils.make_repo_name(repo)
        self.diffusion_repo = self._get_repo(self._search_diffusion())

    def archive(self):
        if self.diffusion_repo is None:
            print(f'Phab: "{self.repo_name}" not found')
            return
        self._archive()

    def _get_repo(self, diffusion_repos):
        """
        Take search results from diffusion and find the repo we want
        """
        for diffusion_repo in diffusion_repos:
            if (diffusion_repo['fields']['name'] == self.repo or
                    diffusion_repo['fields']['name'] == self.repo_name):
                return diffusion_repo
        return None

    def _search_diffusion(self):
        repos = self.phab.diffusion.repository.search(
            constraints={'query': self.repo_name},
            attachments={'projects': True}
        )
        return repos.get('data', [])

    def _make_description(self):
        new_description = "[ARCHIVED]"
        if self.diffusion_repo['fields'].get('description'):
            old_description = self.diffusion_repo['fields']['description']['raw']
            return f"{new_description} {old_description}"
        return new_description

    def _archive(self):
        new_description = self._make_description()
        short_name = self.diffusion_repo['fields']['callsign']
        url = f"https://phabricator.wikimedia.org/diffusion/{short_name}/manage/"
        print(f"Phab: Found {url}")
        print(f"Phab: Archive {self.repo_name}")
        print(f"Phab: Updated description to {new_description}")
        if utils.dry_run(DRY_RUN):
            print("\tDry run, skipping...")
            return
        self.phab.diffusion.repository.edit(
            objectIdentifier=self.diffusion_repo['phid'],
            transactions=[{
                'type': 'description',
                'value': new_description
            }, {
                'type': 'status',
                'value': 'inactive'
            }]
        )

def build_archiver(args, token=None):
    if args.yes:
        global DRY_RUN
        DRY_RUN = False
    try:
        phab = Phabricator(host=HOST, token=token)
    except phabricator.APIError:
        raise SystemExit('Phab: Invalid token')
    phab.update_interfaces()

    return PhabArchiver(args.repo, phab)

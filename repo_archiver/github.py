from ghapi.all import GhApi
import fastcore  # dependency of ghapi

from . import utils

DRY_RUN = True

class GitHubArchiver:
    def __init__(self, repo, api):
        self.repo = utils.make_repo_name(repo)
        self.api = api

    def archive(self):
        """
        - Find the repo on GitHub (or fail)
        - Archive the repo (if not dry run)
        Uses ghapi: <https://ghapi.fast.ai/fullapi.html>
        """
        try:
            repo = self.api.repos.get('wikimedia', self.repo)
        except fastcore.net.HTTP401UnauthorizedError:
            raise SystemExit('GitHub: Invalid token')
        except fastcore.net.HTTP404NotFoundError:
            print(f'GitHub: "wikimedia/{self.repo}" not found')
            return
        if repo is None:
            print(f'GitHub: "wikimedia/{self.repo}" not found')
            return
        if repo['archived']:
            print(f'GitHub: "wikimedia/{self.repo}" is archived')
            return
        print(f"GitHub: Archive {repo['html_url']}")
        if utils.dry_run():
            print("\tDry run, skipping...")
            return
        self.api.repos.update('wikimedia', repo['name'], archived=True)


def build_archiver(args):
    if args.yes:
        global DRY_RUN
        DRY_RUN = False
    api = GhApi()
    return GitHubArchiver(utils.make_repo_name(args.repo), api)

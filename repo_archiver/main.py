import argparse
import os
import textwrap

from . import gerrit
from . import phab
from . import github

def parse_args():
    ap = argparse.ArgumentParser()
    ap.add_argument("repo", help="Repo name (e.g. 'myrepo/repo')")
    ap.add_argument("--task", help="")
    ap.add_argument("--yes", help="Archive without prompting",
                    action="store_true")
    ap.add_argument("-r", "--gitlab-repo",
                    help="GitLab repo name (e.g. 'myrepo/repo')")
    ap.add_argument("--no-gerrit", help="Don't archive the gerrit repo",
                    action="store_true")
    ap.add_argument("--no-phab", help="Don't archive the Phab repo",
                    action="store_true")
    ap.add_argument("--no-github", help="Don't archive the GitHub repo",
                    action="store_true")
    args = ap.parse_args()

    return args

def check_creds(no_gerrit, no_phab, no_github):
    try:
        if not no_gerrit:
            os.environ['GERRIT_USER']
            os.environ['GERRIT_TOKEN']
        if not no_phab:
            os.environ['PHAB_TOKEN']
        if not no_github:
            os.environ['GITHUB_TOKEN']
    except KeyError:
        raise SystemExit(textwrap.dedent('''
        Please set the following environment variables:
        - GERRIT_USER
        - GERRIT_TOKEN
        - PHAB_TOKEN (or set --no-phab)
        - GITHUB_TOKEN (or set --no-github)'''))

def main():
    args = parse_args()
    no_phab = args.no_phab
    no_github = args.no_github
    if args.gitlab_repo:
        no_phab = True
        no_github = True
        if args.gitlab_repo and not (args.no_github and args.no_phab):
            print("Skipping Phab and GitHub repos. Migration to GitLab not yet supported")
    check_creds(args.no_gerrit, no_phab, no_github)
    if not args.no_gerrit:
        gerrit.build_archiver(
            args,
            os.environ['GERRIT_USER'],
            os.environ['GERRIT_TOKEN']
        ).archive()
    if not no_phab:
        phab.build_archiver(
            args,
            os.environ['PHAB_TOKEN']
        ).archive()
    if not no_github:
        github.build_archiver(args).archive()

if __name__ == "__main__":
    main()

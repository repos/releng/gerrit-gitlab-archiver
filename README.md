repo archiver
=============

This script is used to archive a Gerrit repository for Gerrit, Diffusion, and GitHub.

USAGE
-----

    archiver.py [-h] [--task TASK] [--yes] [--no-gerrit] [--no-phab] [--no-github] [-r GITLAB_REPO] repo

    arguments:
     repo                        Gerrit repo to archive (e.g. 'myrepo/repo')
     --yes                       Run without prompting
     --no-gerrit                 Don't archive the Gerrit repo
     --no-phab                   Don't archive the Phab repo
     --no-github                 Don't archive GitHub repo
     --task           (optional) Phabricator task number
     --gitlab-repo    (optional) GitLab repo name (e.g. 'myrepo/repo')

ENVIRONMENT VARIABLES
---------------------

1. `GERRIT_USER`: Gerrit username
2. `GERRIT_TOKEN`: Gerrit http token
3. `GITHUB_TOKEN`: GitHub http token
4. `PHABRICATOR_TOKEN`: Phabricator conduit token

EXAMPLES
--------

You can use it to archive a repo by running:

    archiver.py myrepo/repo --task T123456

If you want to note a repo has moved to GitLab, you can run:

    archiver.py myrepo/repo --task T123456 --gitlab-repo repos/myrepo/repo

EXPLANATION
-----------
The script will:

1. Reparent the repo to `All-Archived-Projects`
2. Create a new orphan branch (named either ARCHIVED or MOVED_TO_GITLAB)
   with a README.md file
3. Push that orphan branch to Gerrit
4. Update the repo's HEAD to point to the new orphan branch
5. Update the repo's description to include the archive message and task (if any)
6. Set the repo to read-only
7. Attempt to find and archive any related Phabricator diffusion repos
8. Attempt to find and archive any related GitHub repos

SETUP
-----

This repo depends on third-party python libraries:

- [requests](https://requests.readthedocs.io/en/latest/)
- [phabricator](https://github.com/disqus/python-phabricator)
- [ghapi](https://ghapi.fast.ai/fullapi.html)

To use, setup a virtualenv and install all requirements:

    python3 -m virtualenv .venv
    . .venv/bin/activate
    pip install -r requirements.txt
    ./archive.py --help

TODO
----

1. Link this with the `integration/config:utils/archive-repo.py` script

* * *

* Copyright 2023 Tyler Cipriani <thcipriani@wikimedia.org>
* License: GPLv3
